/**
 * Asn 3
 * Martin Thingvold, mat534, 11225399
 */
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <string.h>
#include <fcntl.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <signal.h>

#include <poll.h>

#define LOCALHOST "127.0.0.1"
#define ROUTERCOUNT 26
#define ASCIIOFFSET 65
#define SHRT_MAX 32767
typedef enum {false,true} bool;

char routerName;
char oneName='\0';
char twoName='\0';
//strings of the ports
char *hostPort;
char *portOne;
char *portTwo;

//booleans
bool bothcmds=false;
bool oneConnected=false;
bool twoConnected=false;

//Global sockfd info for host
struct sockaddr_in me;
int sockfd;

int socketArray[ROUTERCOUNT];
struct sockaddr_in addrArray[ROUTERCOUNT];
int cur=0;

//sockets for cmd line argument routers
int sockOne,sockTwo;

//char next_hop[ROUTERCOUNT][2];
//the distance between this node and all other nodes
short distanceVector[ROUTERCOUNT];
char next_hop[ROUTERCOUNT];
char socketName[ROUTERCOUNT];

//Polls file desscriptor
struct pollfd pfds[ROUTERCOUNT];
int fd_count;

//The message structure that gets sent between routers
struct Message{
	char senderName;
	char neighbor[2];
	short distVec[ROUTERCOUNT+1];
	char portNum[6];
	char nxt_hop[ROUTERCOUNT];
}Message;

int max(int s1, int s2){
	if(s1 > s2)
		return s1;
	return s2;
}

void updateCur(){
	for(int i=0;i<ROUTERCOUNT;i++){
		if(socketArray[i]==0){
			cur=i;
			return;
		}
	}
	printf("Router is full\n");
}

void distanceVectorUpdate(short *array, char sender, char *nxt_hop){
	for(int i=0;i<ROUTERCOUNT;i++){
		if(distanceVector[i]==0){
			continue;
		}
		else if(array[i]==-1 && next_hop[i]==sender){
			distanceVector[i]=-1;
			next_hop[i]='\0';
		}
		if(array[i]!=-1 && 
				((distanceVector[i]==-1 && nxt_hop[i]!=routerName)
				 || array[i]+1<distanceVector[i])){
			distanceVector[i]=array[i]+1;
      		next_hop[i]=sender;
		}
	}
}
void printArray(short* array){
  printf("Name\tDistance\tNext_hop\n");
	for(int i=0;i<ROUTERCOUNT;i++){
		if(array[i]!=-1){
			printf("%c\t%d\t\t%c\n",i+ASCIIOFFSET,array[i],next_hop[i]);
		}
	}

}
int setupSocket(int i){
  int returnval;
  addrArray[i].sin_family=AF_INET;
  if(i==0){
	  addrArray[i].sin_port = htons(atoi(portOne));
  } else if(i==1){
    	  addrArray[i].sin_port = htons(atoi(portTwo));
  }
	//inet_pton(AF_INET,LOCALHOST,&addrArray[0].sin_addr.s_addr);
	addrArray[i].sin_addr.s_addr = inet_addr(LOCALHOST);
	// make a socket:
	returnval=socket(AF_INET, SOCK_STREAM, 0);
	if(sockOne==-1){
		perror("router1: socket");
	}
  return returnval;
}
void add_to_pfds(struct pollfd (*pf)[26], int newfd, int *fdc){
	if(*fdc == ROUTERCOUNT){
		printf("ERROR Can't add to pfds, it's full!\n");
	}
	(*pf)[*fdc].fd = newfd;
	(*pf)[*fdc].events=POLLIN;

	(*fdc)++;
}

void del_from_pfds(struct pollfd pf[], int i, int *fdc){
	pf[i]=pf[*fdc-1];
	(*fdc)--;
}

void next_hop_degrade(char thanosSnap){
  for(int i=0; i<ROUTERCOUNT; i++){
    if(next_hop[i]==thanosSnap){
      distanceVector[i]=-1;
      next_hop[i]='\0';
    }
  }
}

struct Message initMsg(){
	struct Message msg;
	short networkArray[ROUTERCOUNT];
	msg.senderName=routerName;
	msg.neighbor[0]='\0';
	msg.neighbor[1]='\0';
	//portNum is only used in specific cases;
	msg.portNum[0]='\0';
	/*
	   for(int i=0; i< ROUTERCOUNT; i++){
	   if(distanceVector[i]==-1){
	   msg.distVec[i]=='!';
	   }else{
	   msg.distVec[i]=(char)distanceVector[i];
	   }
	   }
	   msg.distVec[ROUTERCOUNT+1]='\0';
	   */
	//convert distanceVector to network doable
	for(int i=0;i<ROUTERCOUNT;i++){
		networkArray[i] = htons(distanceVector[i]);
	}
	for(int i=0; i<ROUTERCOUNT;i++){
		msg.distVec[i]=networkArray[i];
		msg.nxt_hop[i]=next_hop[i];
	}
	
	return msg;
}
//Parses arguments for use by program
//Makes sure no laws are broken ;)
void ParseArgs(int argc, char *argv[]){
	if(argc < 4){
		printf("Error: too few arguments given");
		exit(1);
	}
	if(argc > 5){
		printf("Error: too many arguments given\n");
		printf("Format: router name hostport port1 port2\n");
		exit(1);
	}
	//get Router Name from argv
	routerName=argv[1][0];
	if(routerName<'A' || routerName>'Z'){
		printf("Router name is not between A and Z.\n");
		exit(1);
	}
	//oddly place distanceVector bit
	for(int i=0; i<ROUTERCOUNT; i++){
		if(i!=routerName%ASCIIOFFSET){
			distanceVector[i]=-1;
		}
	}
	//get the hosts port from argv
	hostPort=malloc(sizeof(argv[2]));
	hostPort=argv[2];
	if(atoi(hostPort) < 30000 || atoi(hostPort) > 40000){
		printf("Error: port number not supported\n");
		exit(1);
	}
	//get target port one from argv
	portOne=malloc(sizeof(argv[3]));
	portOne=argv[3];
	if(atoi(portOne) < 30000 || atoi(portOne) > 40000){
		printf("Error: port number not supported\n");
		exit(1);
	}
	//get other target port (if specified) from argv
	if(argc == 5){
		bothcmds=1;
		portTwo=malloc(sizeof(argv[4]));
		portTwo=argv[4];
		if(atoi(portTwo) < 30000 || atoi(portTwo) > 40000){
			printf("Error: port number not supported\n");
			exit(1);
		}
	}
}


void pollHelper(){

	struct Message msg=initMsg();
	int nbytes;
	for(int i=0; i< fd_count; i++){
		if(pfds[i].revents & POLLIN){
			//wegotem
			if(pfds[i].fd==sockfd){
				//handle new conns
				//
				int temp;
				socklen_t size = sizeof me;
				temp = accept(sockfd, (struct sockaddr*) &addrArray[cur],
						&size);
				if(temp == -1){
					perror("router:accept");
					memset(&addrArray[cur],0,sizeof(addrArray[cur]));
				}
				else{
					add_to_pfds(&pfds,temp,&fd_count);

					if(recv(temp, &msg,sizeof(msg),0)==-1){
						perror("recv handshake");
					}
					//check if the router is the same as a cli arg
					if(strcmp(msg.portNum,portOne)==0){
						oneConnected=true;
						sockOne=temp;
					}else if(bothcmds && strcmp(msg.portNum,portTwo)==0){
						twoConnected=true;
						sockTwo=temp;
					}else{
						socketArray[cur]=temp;
            socketName[cur]=msg.senderName;
						updateCur();
					}


					msg.senderName=routerName;
					if (msg.neighbor[0]=='?'){
						msg.neighbor[0]='!';
						strcpy(msg.portNum,hostPort);

						if(send(temp,&msg,sizeof(msg),0)==-1){
							perror("recv handshook");
						}
					}else if(msg.neighbor[1]=='?'){
						msg.neighbor[1]='!';
						if(send(temp,&msg,sizeof(msg),0)==1){
							perror("recv handshook");
						}
					}


				}

			}
			else{
				if((nbytes = recv(pfds[i].fd, &msg, sizeof msg, 0))<=0){
					//got error/cnxn closed
					//
					if(nbytes==0){
						//connection closed
						printf("socket hung up\n");
            

            if(pfds[i].fd==sockOne){
              oneConnected=false;
              next_hop_degrade(oneName);
			  close(pfds[i].fd);
              sockOne=setupSocket(0);
            }else if(pfds[i].fd==sockTwo){
              twoConnected=false;
              next_hop_degrade(twoName);
			  close(pfds[i].fd);
              sockTwo=setupSocket(0);
            }else{
              for(int k=0;k<ROUTERCOUNT;k++){
                if(pfds[i].fd==socketArray[k]){
				  socketArray[k]=0;
                  next_hop_degrade(socketName[k]);
                }
              }
			  close(pfds[i].fd);
            }
					}else{
						perror("recv");
					}
					del_from_pfds(pfds,i,&fd_count);
				}
				else{
					if(msg.neighbor[0]=='!'){
						oneName=msg.senderName;
						distanceVector[oneName%ASCIIOFFSET]=1;
            next_hop[oneName%ASCIIOFFSET]=msg.senderName;
					}else if(msg.neighbor[1]=='!'){
						twoName=msg.senderName;
						distanceVector[twoName%ASCIIOFFSET]=1;
            next_hop[twoName%ASCIIOFFSET]=msg.senderName;
					}else{
						for(int i=0; i<ROUTERCOUNT;i++){
							msg.distVec[i]=ntohs(msg.distVec[i]);
						}
						distanceVectorUpdate(msg.distVec,msg.senderName,
								msg.nxt_hop);
					}
				}

			}
		}
	}
}



void RouterLoop(){
	//struct sockaddr_in sockaddrOne, sockaddrTwo;
	int rv;

	//set timeval for use in select


	//make socket for first port specified in argv
  sockOne=setupSocket(0);
	
	//if two routers were specified create a second socket
	if(bothcmds){
		sockTwo=setupSocket(1);
	}

	add_to_pfds(&pfds,sockfd,&fd_count);

  /*
	pfds[0].fd=sockfd;
	pfds[0].events = POLLIN;
	fd_count =1;
  */
	for(;;){
		listen(sockfd,10);
		if((rv=poll(pfds,fd_count,2000))==-1){
			perror("poll");
		}
		else if(rv==0){
			//print to stdout Dx(y) and next_hop(y) for all routers y that x
			// is aware of and thinks it has a route to.
			printArray(distanceVector);
      sleep(1);
			for(int i=0; i < ROUTERCOUNT; i++){
				struct Message msg=initMsg();

				//test message
				if(socketArray[i]==0){
					continue;
				}

				//if(error_code!=0 && send(socketArray[i],newmsg,sizeof(newmsg),0)==-1){
				if( send(socketArray[i],&msg,sizeof(msg),0)==-1){
					perror("server: send");
					socketArray[i]=0;
					memset(&addrArray[i],0,sizeof(addrArray[i]));
				}
				//test output to see where the msg is sending
				//printf("Sent?: %d\n",i);
			}
			//sleep(0.8);

			//Attempt to create a tcp connection with each cli arg port
			if (oneConnected==0){
				if( connect(sockOne,
							(struct sockaddr*)&addrArray[0],
							sizeof(addrArray[0])) == -1) {
					perror("1st router: connect");
          sockOne=setupSocket(0);
				}else{
					add_to_pfds(&pfds,sockOne,&fd_count);
					struct Message msg=initMsg();
					msg.senderName=routerName;
					msg.neighbor[0]='?';
					strcpy(msg.portNum,hostPort);
					if(send(sockOne,&msg,sizeof(msg),0)==-1){
						perror("init handshake");
					}
					updateCur();
					oneConnected=1;
				}
			}else{
				struct Message msg=initMsg();

				//send message to first router of current distancevector info
				//and whatnot
				if(send(sockOne,&msg,sizeof(msg),0)==-1){
					perror("sending distVec to router 1");
          oneConnected=false;
            if(oneName!='\0'){
				next_hop_degrade(oneName);
				oneName='\0';
            }
            sockOne=setupSocket(0);
				}
			}
			//checks if the second argument is specified and connects to it
			if (twoConnected==0 && bothcmds){
				if (connect(sockTwo,
							(struct sockaddr*)&addrArray[1],
							sizeof(addrArray[1])) == -1){
					perror("2nd router: connect");

				}else{
					add_to_pfds(&pfds,sockTwo,&fd_count);
					struct Message msg=initMsg();
					msg.senderName=routerName;
					msg.neighbor[1]='?';
					if(send(sockTwo,&msg,sizeof(msg),0)==-1){
						perror("init handshake");
					}

					updateCur();
					twoConnected=1;
				}
			}else if(bothcmds){
				struct Message msg=initMsg();
				//send message to first router of current distancevector info
				//and whatnot
				//msg.distVec=distanceVector;

				if(send(sockTwo,&msg,sizeof(msg),0)==-1){
					perror("sending distVec to router 2");
					twoConnected=false;
            if(twoName!='\0'){
				next_hop_degrade(twoName);
            twoName='\0';
            }
            sockTwo=setupSocket(0);
				}
			}
			}else{
				pollHelper();
			}
		}
	}

  void signal_callback(int signum){
    printf("Caught SIGPIPE %d\n",signum);
  }
	//int main(int argc, char *argv[]){
	int main(int argc, char *argv[]){

		ParseArgs(argc,argv);
    //ignoring sigpipes
    signal(SIGPIPE,signal_callback);
		// first, load up address structs with getaddrinfo():

		me.sin_family=AF_INET;
		me.sin_port = htons(atoi(hostPort));
		inet_pton(AF_INET,LOCALHOST,&me.sin_addr.s_addr);
		// make a socket:
		sockfd = socket(PF_INET, SOCK_STREAM, 0);


		// bind it to the port we passed in to getaddrinfo():
		bind(sockfd, (struct sockaddr*)&me, sizeof(me));

    //set next_hop value for routerName
    next_hop[routerName%ASCIIOFFSET]=routerName;

		//addrArray[routerName%ASCIIOFFSET]=me;
		//run the routerLoop code, moved to its own function to make
		//this file a little less messy.
		RouterLoop();


		return 0;

	}
