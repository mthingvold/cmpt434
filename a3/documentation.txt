#Martin Thingvold
#mat534, 11225399
#CMPT434 ASN 3
#3/21/2020

Documentation

DEFINITIONS:
    LOCALHOST just address localhost as a string nothing fancy
    ROUTERCOUNT the amount of max routers I can have (set to 26 to follow)
    assignment description
    ASCIIOFFSET the offset of how many characters "A" is off from 0 so if you
    want to represent the letter in an array you can shift it with modulus
    SHRT_MAX the max value of a short int, used to indicate a dropped connection.

BOOL TYPE: I think this is self explanatory.

Message Structure:
    this is the structure I use for all communication between routers.
    its contents are as follows:
    senderName: the name(char) of the sending router
    neighbor: this is to confirm that a router was specified by an ARGument
    distVec: the router's current distance vector
    portNum: a string of the port number to check if it's in a routers cli args

GLOBAL VARIABLES:
    routerName: the router's name, specified in cli argument and set when
    parsing command line arguments
    oneName: the name of the first cli specified router
    twoName: the name of the second cli specified router (if applicable)
    hostPort: the routers port that it's bound to
    portOne: the router's first connection specified in cli
    portTwo: the router's second connection specified in cli(if applicable)

    bothcmds: true if two outgoing ports were specified when calling router
    oneConnected: true if a connection exists between router and sockOne
    twoConnected: true if a connection exists between router and sockTwo

    me: sockaddr_in of the router
    sockfd: router's socket, accepts incoming connections
    socketArray: an array of incoming sockets
    addrArray: and array of incoming connections address info
    
    cur: always set so that socketArray[cur] will be an unset socket.

    sockOne: socket for outgoing port 1
    sockTwo: socket for outgoing port 2

    distanceVector: the routers current distanceVector
    next_hop: the next hop router has to take to reach index.
	socketName: this array keeps track of which socket has which name

int max(int s1, int s2):
    trivial, returns the greater number
void updateCur():
    Loops through socketArray and finds the first entry that equals zero.
    Sets cur to that so cur never overwrites a socket.

void distanceVectorUpdate(short *array):
    updates distanceVector comparing itself with
    a newly passed message and taking either the better result, or SHRT_MAX if the connection was disconnected. Also updates next_hop info.

void printArray(short* array):
    a helper function to print the array and its
    next_hop info.

int setupSocket(int i):
	returns a new socket that gets set up by the function.
	Should only be used when i=0 or 1
	because it only sets up the to CLI arg sockets

void add_to_pfds(struct pollfd (*pf)[26], int newfd, int *fdc):
    credit to Beej's guide.
    adds a new poll file descriptor and updates
    the current file descriptor count.

void del_from_pfds(struct pollfd pf[], int i, int *fdc):
    removes current file descriptor and decrements
    current file descriptor count

void next_hop_degrade(char thanosSnap):
	loops through and finds which routers will be
	affected by a disconnection and updates distancvector/nexthop accordingly
	sorry about the weird variable name, it's the last thing i wrote and I
	wanted it to be a little fun :p


struct Message initMsg():
    creates a new Message struct and initializes it with values.
    sets each value of distVect to htons of its corresponding distanceVector
    to avoid horrific endian signedness errors

ParseArgs:
    it does argument parsing,
    checks that the correct amount of arguments are passed.
    checks that routername is correct
    NOTE: if multiple letters are specified in routerName 
	ParseArgs sets it to the first letter.
    initializes distanceVector values to -1, besides the router itself which
    remains 0
    checks that the ports are between 30000 and 40000
    sets bothcmds if outgoing port 2 is specified

void pollHelper():
    this function runs when poll()'s call has something on a file descriptor.
    if the active filedescriptor is sockfd we have an incoming connection
    that needs to be accepted, so we accept it.
    After accepting it we add it's file descriptor to pfds. Then we receive
    initial data from the connector in what I dub the "initial handshake"
    we check the received message to see if it's coming 
    from either outgoing port and if so we set our connection
    value to true and set the appropriate socket
    if it's an incoming connection with an unknown port to the router
	as well as setting the socketsName in the array for later socket degrades.

    It then replies to the send its info so that the sender can set its
    oneName/twoName variables accordingly.


    IF the fd is not an incoming connection it's an incoming message
    in this case we first check that the neighbor value is set. 
    if it is update oneName/twoName appropriately.
    if neither neighbor flags are set run ntohs on each part of the messages
    distance vector and update the distanceVector
    accordingly.


RouterLoop():
  RouterLoop sets up the addresses for port one and if applicable port two.
  add socfd to pfds

  then enter an infinite loop.
  the first thing the loop does is listen for incoming connections.
  after this it uses poll to see if any of its sockets have data that needs
  to be addressed (if there is data check pollHelper)
  if poll times out (after two seconds) it first
  prints the information of the current distance to other routers
  and the next_hop information.
  then for each router that connected to it (that weren't specified in the
   command line args)
   it sends its current distanceVector information to update the listening routers'

   if either outgoing port is not connected the router will attempt to connect to it.
   if a connection is established the router will then send a message with the
   neighbor flag set so that it can adequately set oneName and twoName
   if the connection fails the socket gets overwritten with a new one,
   it shouldn't be needed but it did fix an annoying issue.

   if either outgoing port is connected the router sends
   its data.
   if those sends fail the router updates it's table and begins attempting
   to connect to those routers specified in the cli again.


main():
    takes in command line arguments, sets up the local address
    assigns sockfd a socket.
    binds sockfd.
    begins routerLoop
	also sigpipe is set to just return the error occuring, because the program
	should be able to recover from a SIGPIPE


ISSUES:
	In all honesty my testing hasn't been incredibly extensive. So it's
	reasonable to assume that there are issues that I haven't accounted for.
	The only issue I have found with it while testing was a recv() hang on
	some routers when they connect at a specific time, but since i fixed
	a seperate error I haven't experienced this issue again.
