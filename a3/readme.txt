#Martin Thingvold
#mat534, 11225399
#CMPT434 ASN 3
#3/21/2020

README.txt

To build the program:
make clean && make

To run the program:
./router <ROUTERNAME> <ROUTERPORT> <OUTGOINGPORT1> <OUTGOINGPORT2>

note: specifying OUTGOINGPORT2 is optional
      ROUTERNAME must be a capital english alphabet letter.

close the program by sending a SIG-INT signal(usually ctrl+c)

Part B questions are contained in mat534a3partb.pdf

tex source file included if that is something that interests you.
