#ifndef QUEUE_H
#include <stdint.h>
#define QUEUE_H
#define MSGSIZE 140
#define QUEUESIZE 32
struct Queue{
	char array[QUEUESIZE][MSGSIZE];
	uint32_t idx;
}Queue;

struct Queue * InitQueue();
//void removeQueue();
void enqueue(struct Queue *q, char *msg);
void dequeue(struct Queue *q, char *msg);
#endif
