/*
 ** listener.c -- a datagram sockets "server" demo
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <sys/time.h>
#include <time.h>

#define BUFFERLENGTH 120
#define DIVIDER ":"

float corruptionChance;
char *receiverPort;
struct addrinfo hints, *servinfo, *p;
char buffer[BUFFERLENGTH];
uint32_t currentMsg;

void RecvLoop(int sockfd){
	struct sockaddr_storage their_addr;
	socklen_t addr_len = sizeof(their_addr);
	char bufferCopy[BUFFERLENGTH];
	//token used for strtok function
	char *token;
	//sequence number received
	uint32_t seqNum;
	float randomVal;
	srand((unsigned int)time(NULL));
	currentMsg=0;
	while(1){
		printf("listener: waiting to recvfrom...\n");
		memset(buffer,0,BUFFERLENGTH);

		if ((recvfrom(sockfd, buffer, BUFFERLENGTH , 0,
						(struct sockaddr *)&their_addr, &addr_len)) == -1) {
			perror("recvfrom");
			exit(1);
		}
		//add null character
		buffer[BUFFERLENGTH]='\0';
		strcpy(bufferCopy,buffer);
		token = strtok(bufferCopy,DIVIDER);
		seqNum=atoi(token);
		if(seqNum==currentMsg){
			printf("%s:\tEnter Acknowledgement:\t",buffer);
			//read input for Y
			fgets(bufferCopy,BUFFERLENGTH,stdin);
			randomVal= (float)rand()/RAND_MAX;
			if(bufferCopy[0]=='Y' || bufferCopy[0]=='y' ){
				if(randomVal<corruptionChance){
					//probablility skill check
					if(sendto(sockfd,&(currentMsg),sizeof(uint32_t),0,
						(const struct sockaddr *)&their_addr,addr_len)==-1){
				perror("talker: sendto");
				exit(1);
			}
					currentMsg++;
				} else{
					printf("Message randomly corrupted\n");
				}	

			}
		}
		else if(seqNum>currentMsg){
			printf("OUT OF ORDER MESSAGE: %s",buffer);
		}
		else if(seqNum<currentMsg){
			//currentMsg=buffer[0];
			printf("Retransmission Message: %s",buffer);
			if(sendto(sockfd,&(currentMsg),sizeof(uint32_t),0,
						(const struct sockaddr *)&their_addr,addr_len)==-1){
				perror("talker: sendto");
				exit(1);
			}
			
		}
	}
}
void ParseArgs(int argc, char *argv[]){
	if(argc!=3){
		printf("incorrect amount of arguments given\n");
		exit(1);
	}
	receiverPort=malloc(sizeof(argv[2]));
	receiverPort=argv[1];
	if(atoi(receiverPort)>40000 || atoi(receiverPort)<30000){
		printf("Error port number not in allowed range 30,000 - 40,000");
		exit(1);
	}
	corruptionChance=strtof(argv[2],NULL);
	if(corruptionChance > 1 || corruptionChance < 0){
		printf("corruption chance out of bounds");
		exit(1);
	}
}
// get sockaddr, IPv4 or IPv6:
void *get_in_addr(struct sockaddr *sa)
{
	if (sa->sa_family == AF_INET) {
		return &(((struct sockaddr_in*)sa)->sin_addr);
	}

	return &(((struct sockaddr_in6*)sa)->sin6_addr);
}

int main(int argc, char *argv[])
{
	int sockfd;
	int rv;
	ParseArgs(argc,argv);

	memset(&hints, 0, sizeof hints);
	hints.ai_family = AF_UNSPEC; // set to AF_INET to force IPv4
	hints.ai_socktype = SOCK_DGRAM;
	hints.ai_flags = AI_PASSIVE; // use my IP

	if ((rv = getaddrinfo(NULL, receiverPort, &hints, &servinfo)) != 0) {
		fprintf(stderr, "getaddrinfo: %s\n", gai_strerror(rv));
		return 1;
	}

	// loop through all the results and bind to the first we can
	for(p = servinfo; p != NULL; p = p->ai_next) {
		if ((sockfd = socket(p->ai_family, p->ai_socktype,
						p->ai_protocol)) == -1) {
			perror("listener: socket");
			continue;
		}

		if (bind(sockfd, p->ai_addr, p->ai_addrlen) == -1) {
			close(sockfd);
			perror("listener: bind");
			continue;
		}

		break;
	}

	if (p == NULL) {
		fprintf(stderr, "listener: failed to bind socket\n");
		return 2;
	}

	freeaddrinfo(servinfo);


	RecvLoop(sockfd);


	close(sockfd);

	return 0;
}