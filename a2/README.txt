#Martin Thingvold
#mat534, 11225399
#Assignment 2 CMPT 434
#README
make simply with make.
make clean will remove the compiled binaries

to run the sender
	./sender <SERVER ADDR> <PORT NUMBER> <Window Size> <timeout in seconds>o

to run the receiver
	./receiver <PORT NUMBER> <Probability between 0 and 1>

to run the forwarder
	./forwarder <PORT TO BIND> <IP ADDR> <Port of recvr/fwdr> <window size> <timeout>

Documentation is in Documentation.txt
Part B is in mat534-partb-a2.pdf
tex file provided if you like those.
