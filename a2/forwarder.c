#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <time.h>

#include "queue.h"
#define BUFFERLENGTH 120
#define DIVIDER ":"

char *fwdPort;

char *receiverPort;
struct addrinfo hints, *servinfo, *p;
char buffer[BUFFERLENGTH];
uint32_t currentMsg;
char *senderPort;  // the port users will be connecting to
uint32_t windowSize;
int timeout;
char sendBuffer[BUFFERLENGTH];
struct Queue queue;
struct addrinfo *sender, *receiver;
uint32_t numAcks;
uint32_t lastAcknowledgment;


void FwdLoop(int sockfd, int recvfd){
	struct sockaddr_storage their_addr;
	socklen_t addr_len = sizeof(their_addr);
	char bufferCopy[BUFFERLENGTH];
	int sReturn;
	//token used for strtok function
	char *token;
	//sequence number received
	uint32_t seqNum=0;
	srand((unsigned int)time(NULL));
	currentMsg=0;
	struct timeval tv;
	int lastSendTime=(int)time(NULL);
	uint32_t curSeqNum=0;
	fd_set readfs;
	while(1){
		FD_ZERO(&readfs);
		FD_SET(sockfd, &readfs);
		FD_SET(recvfd,&readfs);

		tv.tv_sec=timeout-((int)time(NULL)-lastSendTime);
		printf("forwarder: waiting to recvfrom...\n");
		memset(buffer,0,BUFFERLENGTH);
		//if queue is empty wait
		if(queue.idx==0){
			sReturn=select(recvfd+1, &readfs, NULL, NULL, NULL);

		}else{
			sReturn=select(recvfd+1, &readfs, NULL, NULL, &tv);

		}
		if(sReturn <0 ){
			perror("select");
			//idk why this is 4 beej told me to
			exit(4);
		}
		//Sender timed out, retransmit windows
		else if(sReturn ==0){
			if((strlen(queue.array[0])!=0)||lastAcknowledgment<seqNum){
				for(unsigned int i=0; i<queue.idx && i<windowSize;i++){
					strcpy(sendBuffer,queue.array[i]);
					if(send(recvfd,sendBuffer,strlen(sendBuffer),0)==-1){
						perror("talker:sendto");
						exit(1);
					}
					lastSendTime=((int)time(NULL));
				}
			}

		}else{
			if(FD_ISSET(sockfd,&readfs)){

				if ((recvfrom(sockfd, buffer, BUFFERLENGTH , 0,
								(struct sockaddr *)&their_addr, &addr_len)) == -1) {
					perror("recvfrom");
					exit(1);
				}

				//add null character
				buffer[BUFFERLENGTH]='\0';
				strcpy(bufferCopy,buffer);
				token = strtok(bufferCopy,DIVIDER);
				seqNum=atoi(token);
				if(seqNum==currentMsg){
					printf("%s:\tEnter Acknowledgement:\t",buffer);
					//read input for Y
					fgets(bufferCopy,BUFFERLENGTH,stdin);
					if(bufferCopy[0]=='Y' || bufferCopy[0]=='y' ){
						enqueue(&queue,buffer);
						if(currentMsg<windowSize){
							if(send(recvfd,buffer,strlen(buffer),0)==-1){
								perror("talker:sendto");
								exit(1);
							}
							lastSendTime=((int)time(NULL));

						}
						curSeqNum++;
						currentMsg++;
						
					}
				}
				else if(seqNum>curSeqNum){
					printf("OUT OF ORDER MESSAGE: %s",buffer);
				}
				else if(seqNum<curSeqNum){
					//currentMsg=buffer[0];
					printf("Retransmission Message: %s",buffer);

				}
			}
			if(FD_ISSET(recvfd,&readfs)){
				//receive int and dequeue that many times
				if ((recv(recvfd, &numAcks, sizeof(uint32_t) , 0)) == -1) {
					perror("recvfrom");
					exit(1);
				}
				//set the buffer to 0 before assigning it the queue
				if(lastAcknowledgment!=numAcks || strlen(queue.array[0])!=0){
					memset(sendBuffer,0,sizeof(sendBuffer));
					dequeue(&queue,sendBuffer);
					currentMsg--;
						seqNum=atoi(token)+1;
						strcpy(sendBuffer,queue.array[0]);
						if(sendto(sockfd,&numAcks,sizeof(uint32_t),0,
									(const struct sockaddr *)&their_addr,addr_len)==-1){
							perror("talker:sendto");
							exit(1);
									}
						lastSendTime=((int)time(NULL));
						seqNum++;
					

					currentMsg--;
					//pretty sure this is useless but i'm sleep deprived so 
					


					printf("received something\n");	currentMsg++;
				}
			}
		}

	}
}


/**
 * Parses arguments for use by program
 * argv[0] the fwd port
 * argv[1] The IP address of the receiver
 * argv[3] The Port Number of the receiver
 * argv[4] The maximum sending window size
 * argv[5] timeout value
 */
void ParseArgs(int argc, char *argv[]){
	if(argc < 6){
		printf("Error: too few arguments given");
		exit(1);
	}
	if(argc > 6){
		printf("Error: too many arguments given");
		exit(1);
	}
	fwdPort=argv[1];
	if(atoi(fwdPort) < 30000 || atoi(fwdPort) > 40000){
		printf("Error: port number not supported");
		exit(1);
	}
	//receiverPort=malloc(sizeof(argv[3]));
	receiverPort=argv[3];
	if(atoi(receiverPort) < 30000 || atoi(receiverPort) > 40000){
		printf("Error: port number not supported");
		exit(1);
	}
	windowSize=atoi(argv[4]);
	if(windowSize<2){
		printf("Window size cannot be less than two"); 
		exit(1);
	}
	timeout=atoi(argv[5]);
	if(timeout<0){
		printf("timeout cannot be less than 0");
		exit(1); }

}


int main(int argc, char *argv[]) {
	int sockfd,recvfd;
	int rv;
	ParseArgs(argc,argv);

	memset(&hints, 0, sizeof hints);
	hints.ai_family = AF_INET; // set to AF_INET to force IPv4
	hints.ai_socktype = SOCK_DGRAM;
	hints.ai_flags = AI_PASSIVE; // use my IP

	if ((rv = getaddrinfo(NULL, argv[1], &hints, &servinfo)) != 0) {
		fprintf(stderr, "getaddrinfo: %s\n", gai_strerror(rv));
		return 1;
	}

	// loop through all the results and bind to the first we can
	for(p = servinfo; p != NULL; p = p->ai_next) {
		if ((sockfd = socket(p->ai_family, p->ai_socktype,
						p->ai_protocol)) == -1) {
			perror("listener: socket");
			continue;
		}

		if (bind(sockfd, p->ai_addr, p->ai_addrlen) == -1) {
			close(sockfd);
			perror("listener: bind");
			continue;
		}

		break;
	}

	if (p == NULL) {
		fprintf(stderr, "listener: failed to bind socket\n");
		return 2;
	}
	memset(&hints, 0, sizeof hints);
	hints.ai_family = AF_INET; // set to AF_INET to force IPv4
	hints.ai_socktype = SOCK_DGRAM;
	//freeaddrinfo(servinfo);
	if ((rv = getaddrinfo(argv[2], argv[3], &hints, &receiver)) != 0) {
		fprintf(stderr, "getaddrinfo: %s\n", gai_strerror(rv));
		return 1;
	}
	// loop through all thde results and make a socket
	if ((recvfd = socket(receiver->ai_family, receiver->ai_socktype,receiver->ai_protocol)) == -1) {
		perror("talker: socket");
	}


	if (receiver == NULL) {
		fprintf(stderr, "talker: failed to create socket\n");
		return 2;
	}
	if(connect(recvfd,receiver->ai_addr,receiver->ai_addrlen)==-1){
		perror("connect");
		exit(1);
	}


	FwdLoop(sockfd,recvfd);


	close(sockfd);
}
