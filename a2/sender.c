/*
** sender.c -- a go-back-n sliding sender thingy
* Martin Thingvold, mat534, 11225399
*/

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <time.h>

#include "queue.h"

#define BUFFERLENGTH 120
#define DIVIDER ": \n"
//not sure if this is necessary but beej does it.
#define STDIN 0
char *senderPort;  // the port users will be connecting to
uint32_t windowSize;
int timeout;
char inputBuffer[BUFFERLENGTH];
char sendBuffer[BUFFERLENGTH];
struct Queue queue;
struct addrinfo *sender, *receiver;
uint32_t currentMsg;
uint32_t numAcks;
int lastAcknowledgment;

/**
* Parses arguments for use by program
* argv[0] the command that started the program, probably "sender"
* argv[1] The IP address of the receiver
* argv[2] The Port Number of the receiver
* argv[3] The maximum sending window size
* argv[4] timeout value
*/
void ParseArgs(int argc, char *argv[]){
	if(argc < 5){
		printf("Error: too few arguments given");
		exit(1);
	}
	if(argc > 5){
		printf("Error: too many arguments given");
		exit(1);
	}
	senderPort=malloc(sizeof(argv[2]));
	senderPort=argv[2];
	if(atoi(senderPort) < 30000 || atoi(senderPort) > 40000){
		printf("Error: port number not supported");
		exit(1);
	}
	windowSize=atoi(argv[3]);
	if(windowSize<1){
		printf("Window size cannot be less than one"); 
		exit(1);
	}
	timeout=atoi(argv[4]);
	if(timeout<0){
		printf("timeout cannot be less than 0");
		exit(1); }

}

void senderLoop(int sockfd){
	struct timeval tv;
	fd_set readfs;
	char intstring[5];
	int sReturn;
	int lastSendTime=(int)time(NULL);
	int seqNum=0;
	//guess who's using strtok again :D
	char *token;
	//time related values
	//set the timeout value
	while(1){
		FD_ZERO(&readfs);
		FD_SET(sockfd, &readfs);
		FD_SET(STDIN_FILENO,&readfs);
		tv.tv_sec=timeout-((int)time(NULL)-lastSendTime);
		//if queue is empty wait
		if(queue.idx==0){
			sReturn=select(sockfd+1, &readfs, NULL, NULL, NULL);
		}else{
			sReturn=select(sockfd+1, &readfs, NULL, NULL, &tv);
		}
		if(sReturn <0){
			perror("select");
			//idk why this is 4 beej told me to
			exit(4);
		}
		//Sender timed out, retransmit windows
		else if(sReturn ==0){
			if((strlen(queue.array[0])!=0)||lastAcknowledgment<seqNum){
			for(unsigned int i=0; i<queue.idx && i<windowSize;i++){
				strcpy(sendBuffer,queue.array[i]);
				if(sendto(sockfd,sendBuffer,strlen(sendBuffer),0,
								receiver->ai_addr,receiver->ai_addrlen)==-1){
						perror("talker:sendto");
						exit(1);
					}
				lastSendTime=((int)time(NULL));
			}
			}

		}
		//Check which socket is returned
		//Handle the receive call
		else{
			if(FD_ISSET(sockfd,&readfs)){
				//receive int and dequeue that many times
				if ((recvfrom(sockfd, &numAcks, sizeof(uint32_t) , 0,
								receiver->ai_addr, &(receiver->ai_addrlen))) == -1) {
					perror("recvfrom");
					exit(1);
				}
				//set the buffer to 0 before assigning it the queue
				if(lastAcknowledgment!=(int)numAcks || strlen(queue.array[0])!=0){
				memset(sendBuffer,0,sizeof(sendBuffer));

				dequeue(&queue,sendBuffer);
				token=strtok(sendBuffer,DIVIDER);
				while(atoi(token)!=(int)numAcks){
					dequeue(&queue,sendBuffer);
					token = strtok(sendBuffer,DIVIDER);
				}
				seqNum=atoi(token)+1;
				if(currentMsg>windowSize){
					strcpy(sendBuffer,queue.array[0]);
					if(sendto(sockfd,sendBuffer,strlen(sendBuffer),0,
								receiver->ai_addr,receiver->ai_addrlen)==-1){
						perror("talker:sendto");
						exit(1);
					}
					lastSendTime=((int)time(NULL));
					seqNum++;
				}

				currentMsg--;
				//pretty sure this is useless but i'm sleep deprived so 
				numAcks++;


				printf("received something\n");
				}

			}
			if(FD_ISSET(STDIN_FILENO,&readfs)){
				//printf("Read from stdin\n");
				//read from standard in
				fgets(inputBuffer,BUFFERLENGTH-1,stdin);
				//set int string as the string of currentMsg
				sprintf(intstring,"%d",seqNum);
				//prepend intstring to buffer
				strcpy(sendBuffer,intstring);
				strcat(sendBuffer, DIVIDER);
				strcat(sendBuffer, inputBuffer);
				enqueue(&queue,sendBuffer);
				if(currentMsg<windowSize){
					if(sendto(sockfd,sendBuffer,strlen(sendBuffer),0,
								receiver->ai_addr,receiver->ai_addrlen)==-1){
						perror("talker:sendto");
						exit(1);
					}
					lastSendTime=((int)time(NULL));

				}
				//seqNum is the actual sequence number current message
				//is what we compare to the returned number
				//currentMsg is how we make sure that the window size
				//isn't overflowed
				seqNum++;
				currentMsg++;
			}
		}
	}

}


int main(int argc, char *argv[])
{
	int sockfd;
	struct addrinfo hints;
	int rv;
	//int numbytes;

	ParseArgs(argc, argv);
	queue=*InitQueue();

	memset(&hints, 0, sizeof hints);
	hints.ai_family = AF_UNSPEC;
	hints.ai_socktype = SOCK_DGRAM;

	if ((rv = getaddrinfo(argv[1], senderPort, &hints, &receiver)) != 0) {
		fprintf(stderr, "getaddrinfo: %s\n", gai_strerror(rv));
		return 1;
	}
	// loop through all the results and make a socket
	if ((sockfd = socket(receiver->ai_family, receiver->ai_socktype,receiver->ai_protocol)) == -1) {
		perror("talker: socket");
	}


	if (receiver == NULL) {
		fprintf(stderr, "talker: failed to create socket\n");
		return 2;
	}


	senderLoop(sockfd);

	freeaddrinfo(receiver);

	//printf("talker: sent %d bytes to %s\n", numbytes, argv[1]);
	close(sockfd);

	return 0;
}
