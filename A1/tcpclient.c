/**
 * Martin Thingvold, mat534, 11225399
 */
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <string.h>
#include <unistd.h>
#include<errno.h>
#define MAXDATASIZE 40
#define FAIL "!"
#define SUCC "$"
#define BUFFERLENGTH 140
char *serverPort;
char buffer[BUFFERLENGTH];


//Parses arguments for use by program
//Makes sure no laws are broken ;)
void ParseArgs(int argc, char *argv[]){
	if(argc < 3){
		printf("Error: too few arguments given");
		exit(1);
	}
	if(argc > 3){
		printf("Error: too many arguments given");
		exit(1);
	}
	serverPort=malloc(sizeof(argv[2]));
	serverPort=argv[2];
	if(atoi(serverPort) < 30000 || atoi(serverPort) > 40000){
		printf("Error: port number not supported");
		exit(1);
	}
}
void clientLoop(int sockfd){
	char* token;
  char key[40], value[40];
  char* delim=" \n";
  char bufferCopy[BUFFERLENGTH];
  int bytes_set;

  /* Once client connects to server enter a busy loop*/
  //I'm so sorry about this goto
restartloop: 
  while(1){
	  //set memory values to zero
	  memset(key,0,sizeof(key));
	  memset(value,0,sizeof(value));
	  memset(buffer,0,sizeof(buffer));
	  //Read from stdin a command
	  fgets(buffer, sizeof(buffer), stdin);
	  strcpy(bufferCopy,buffer);
	  token=strtok(buffer,delim);

	  //if command is "quit" then exit the application
	  if(strncmp(token,"quit",MAXDATASIZE)==0){
		  if (send(sockfd, bufferCopy, strlen(bufferCopy), 0) == -1)
			  perror("send");
		  close(sockfd);
      return;
	  }
	  //if command is getall then send the getall command to the server and wait for the data to be returned
	  else if(strncmp(token,"getall",MAXDATASIZE)==0){
		  //cmd=GETALL;
		  if (send(sockfd, bufferCopy, strlen(bufferCopy), 0) == -1)
			  perror("send");

	      while(1){
			  if((bytes_set=recv(sockfd,buffer,BUFFERLENGTH,0))==-1){
			  perror("recv");
			  }
			  if(buffer[0]==FAIL[0]){
				  break;
			  }
			  buffer[bytes_set]='\0';
			  printf("%s",buffer);
		  }

		  
	  }
	  else if(strncmp(token,"getvalue",MAXDATASIZE)==0){
		  //Make sure that the requirements for getvalue are met
		  token = strtok(NULL,delim);
		  if(token==NULL){
			  perror("No value given");
		  }
		  else if(strlen(token)>MAXDATASIZE){
			  perror("Value too large");
		  }else{
			  //Send the buffer to the server
			  if(send(sockfd, bufferCopy, strlen(bufferCopy),0) == -1){
				  perror("send");
			  }

			  //Receive the value for specified key
			  if((bytes_set=recv(sockfd, value, MAXDATASIZE-1,0))==-1){
				  perror("recv");
				  exit(1);
			  }
        if(value[0]=='!'){
          printf("Value lookup failed\n");
        }else{
			  value[bytes_set]='\0';
			  printf("VALUE: %s\n",value);
        }
		  }
	  }
	  //Add a key and value
	  else if(strncmp(token,"add",MAXDATASIZE)==0){
		  token = strtok(NULL,delim);
      //check that key exists
		  if(token==NULL){
			  printf("No arguments provided\n");
			  goto restartloop;
		  }
		  strcpy(key,token);
		  token = strtok(NULL,delim);
	  
      //check that value exists
		  if(token==NULL){
			  printf("No value provided\n");
			  goto restartloop;
		  }
		  strcpy(value,token);
		  if(strlen(key)==0 || token==NULL){
			  printf("error value/key cannot be null\n");
			  goto restartloop;
		  }else if(strlen(key)>MAXDATASIZE || strlen(token)>MAXDATASIZE){
			  printf("error value/key cannot be over %d chars\n",MAXDATASIZE);
			  goto restartloop;

		  }else{
        //send the add key command
			  if(send(sockfd, bufferCopy, sizeof(bufferCopy),0) == -1){
				  perror("send");
			  }
        //receive the success/fail value
        if(recv(sockfd, buffer, BUFFERLENGTH,0)==-1){
          perror("recv");
          exit(1);
        }
        if(strncmp(buffer,SUCC,1)!=0){
          printf("error adding key; is key already in server?\n");
        }else{
          printf("Successful addtion\n");
        }
		  }

	  }
	  else if(strncmp(token,"remove",MAXDATASIZE)==0){
		  token = strtok(NULL,delim);
      //check that a key was given for remove
		  if(token==NULL){
			  printf("No key given");
		  }
      //check that key is correct length
		  else if(strlen(token)>MAXDATASIZE){
			  printf("Key too large");
		  }else{
        //send the buffer to the server
			  if(send(sockfd, bufferCopy, sizeof(bufferCopy),0) == -1){
				  perror("send");if(recv(sockfd,buffer,BUFFERLENGTH,0)==-1){
			  perror("recv");
		  }
			  }
        //Receive the status from the server
        if(recv(sockfd, buffer, BUFFERLENGTH,0)==-1){
          perror("recv");
          exit(1);
        }
        if(strncmp(buffer,SUCC,1)!=0){
          printf("error removing key; is key in server?\n");
        }else{
          printf("Successful removal\n");
        }
		  }
	  }
	  else{
		  printf("Error incorrect input");
	  }
  }
}
int main(int argc, char *argv[]){
	int status,sockfd;
	struct addrinfo hints;
	struct addrinfo *servinfo;

	/* NetCode */
	ParseArgs(argc,argv);
	memset(&hints, 0, sizeof hints);
	hints.ai_family = AF_UNSPEC;
	hints.ai_socktype = SOCK_STREAM;
	hints.ai_flags = AI_PASSIVE;
  //DNS/service name lookup
  //servinfo points to a linked list of addrinfos
	if((status = getaddrinfo(argv[1], serverPort, &hints, &servinfo))!=0){
		fprintf(stderr,"getaddrinfo error: %s\n", gai_strerror(status));
		exit(1);
	}

  //Setup socket
	sockfd=socket(servinfo->ai_family, servinfo->ai_socktype,
			servinfo->ai_protocol);

	if(sockfd==-1){
		perror("socket");
	}

	if(connect(sockfd, servinfo->ai_addr, servinfo->ai_addrlen)==-1){
		close(sockfd);
		perror("Client: connect");
		exit(1);
	}


	clientLoop(sockfd);
  freeaddrinfo(servinfo);

	/* assign address info to local client */
	/*
	   status = getaddrinfo(NULL, clientPort, &hints, &client);
	   if (status != 0) {
	   fprintf(stderr, "getaddrinfo error: %s\n", gai_strerror(status));
	   exit(1);
	   }
	   */
	return 0;

}

