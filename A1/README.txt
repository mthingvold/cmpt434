#Martin Thingvold, mat534, 1225399
#CMPT 434
#Assignment 1
please make sure you're current directory is the one containing all the files.
To build the project run:
    make clean && make
To run the tcp server run:
    ./tcpserver
to run the tcp client and connect to the server run:
    ./tcpclient <Hostname> 30612
    #the reson for 30612 is because 0612 is my birthday :D 
To run the tcp proxy run:
    ./tcpproxy <Hostname> 30612
To run the tcp client and connect to the proxy run:
    ./tcpclient <Hostname> 30507

I'm not going to get part c done, sorry.

for documentation refer to: Documentation.txt

for the answers to part B look at mat534-partb-a1.pdf
tex file provided if you're into that.