/**
 * Martin Thingvold, mat534, 11225399
 */
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <string.h>
#include <fcntl.h>
#include <unistd.h>
#define SPORT "30612"
#define MAXDATASIZE 40
#define ARRAYSIZE 20
#define BACKLOG 10	//how many pending connections queue will hold
#define FAIL "!"
#define SUCC "$"
#define BUFFERLENGTH 140

char buffer[BUFFERLENGTH];
char keyArray[20][MAXDATASIZE];
char valueArray[20][MAXDATASIZE];
int addKey(char* key, char* value){
  int first=1;
  int index=-1;

  //Loop to find if key exists
  for(int i=0;i<ARRAYSIZE;i++){
    if(strncmp(key,keyArray[i],MAXDATASIZE)==0){
      //sendAck(sockfd,FAIL);
      return -1;
    }

    if(strlen(keyArray[i])==0 && first==1){
      first--;
      index=i;
    }
  }
  if(index==-1){
    return -1;
  }
  strcpy(keyArray[index],key);
  strcpy(valueArray[index],value);
  return 0;
}
int getvalue(char* key, char* value){
  for(int i=0; i < ARRAYSIZE;i++){
    if(strncmp(key,keyArray[i],MAXDATASIZE)==0){
      strcpy(value,valueArray[i]);
      return 0;
    }
  }

  return -1;
}
int removeKey(char* key){
  for(int i=0; i < ARRAYSIZE;i++){
    if(strncmp(key,keyArray[i],MAXDATASIZE)==0){
      //keyArray[i][0]='\0';

      printf("key before: %s\n",keyArray[i]);
      memset(keyArray[i],0,strlen(keyArray[i]));
      memset(keyArray[i],0, sizeof(keyArray[i]));
      memset(valueArray[i],0,MAXDATASIZE);
      return 0;
    }
  }
  return -1;
}
int getall(){

  return 0;
}
void ServerLoop(int sockfd){
  int numbytes;
  char* token;
  char* delim=" \n";
  char key[MAXDATASIZE], value[MAXDATASIZE];
  while(1){
    memset(key,0,MAXDATASIZE);
    memset(value,0,MAXDATASIZE);
    memset(buffer,0,BUFFERLENGTH);
    token=NULL;
    if((numbytes=recv(sockfd,buffer,sizeof(buffer),0))==-1){
      perror("recv");
      exit(1);
    }
    buffer[numbytes]='\0';
      printf("%s",buffer);

    token=strtok(buffer," ");

    if(strncmp(token,"quit\n",MAXDATASIZE)==0){
      return;

    }else if(strncmp(token,"getall\n",MAXDATASIZE)==0){

      for(int i=0; i < ARRAYSIZE; i++){
        
        if(strlen(keyArray[i])!=0){
          strcpy(buffer,keyArray[i]);
          strcat(buffer, ":");
          strcat(buffer,valueArray[i]);
          strcat(buffer,"\n");

          if(send(sockfd,buffer,BUFFERLENGTH,0)==-1){
          perror("send");
          exit(1);
        }
        }
      }
      if(send(sockfd,FAIL,BUFFERLENGTH,0)==-1){
          perror("send");
          exit(1);
        }
    }else if(strncmp(token,"getvalue",MAXDATASIZE)==0){
      token=strtok(NULL,delim);
      strcpy(key,token);
      if(getvalue(key, value)==-1){
        printf("key doesn't exist\n");
        if(send(sockfd,FAIL,1,0)==-1){
          perror("send");
          exit(1);
        }
      }
      //send the value back to the client
      else if(send(sockfd,value,sizeof(value),0)==-1){
        perror("send");
        exit(1);
      }

    }else if(strncmp(token,"add",MAXDATASIZE)==0){
      token=strtok(NULL,delim);
      strcpy(key,token);
      token=strtok(NULL,delim);
      strcpy(value,token);
      //add key to array
      if(addKey(key,value)==-1){
        printf("Error adding key\n");
        send(sockfd,FAIL,1,0);
      }
      else if(send(sockfd,SUCC,1,0)==-1){
        perror("send");
        exit(1);
      }

    }else if(strncmp(token,"remove",MAXDATASIZE)==0){
      token=strtok(NULL,delim);
      strcpy(key,token);
      if(removeKey(key)==-1){
        send(sockfd,FAIL,1,0);
        printf("key doesn't exist\n");
      }
      else if(send(sockfd,SUCC,1,0)==-1){
        perror("send");
        exit(1);
      }

    }
    else{
      printf("ERROR: BUFFER TRANSFERRED INCORRECTLY\n Aquired Command: %s",token);
    }
  }       
}

//int main(int argc, char *argv[]){
int main(void){


  int status,sockfd,clientfd;
  socklen_t addr_size;
  struct addrinfo hints, *serverInfo;
  struct sockaddr_storage their_addr;
  memset(keyArray,'\0',sizeof(keyArray[0][0])*ARRAYSIZE*MAXDATASIZE);
  memset(valueArray,'\0',sizeof(valueArray[0][0])*ARRAYSIZE*MAXDATASIZE);

  setbuf(stdout,NULL);
  memset(&hints, 0, sizeof hints);
  hints.ai_family = AF_UNSPEC;
  hints.ai_socktype = SOCK_STREAM;
  hints.ai_flags=AI_PASSIVE;
  /* assign address info to local server */
  status = getaddrinfo(NULL, SPORT, &hints, &serverInfo);
  if (status != 0) {
    fprintf(stderr, "getaddrinfo error: %s\n", gai_strerror(status));
    exit(1);
  }
  sockfd=socket(serverInfo->ai_family,serverInfo->ai_socktype,
      serverInfo->ai_protocol);

  if(bind(sockfd,serverInfo->ai_addr,serverInfo->ai_addrlen)==-1)
  {
    close(sockfd);
    perror("server: bind");
    exit(1);
  }

  if(listen(sockfd,BACKLOG)==-1){
    perror("listen");
    exit(1);
  }

  addr_size = sizeof their_addr;
  clientfd=accept(sockfd, (struct sockaddr *)&their_addr, &addr_size);
  close(sockfd);
  ServerLoop(clientfd);
  freeaddrinfo(serverInfo);
  return 0;

}
