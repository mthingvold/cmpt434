/**
 * Martin Thingvold, mat534, 11225399
 */
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <string.h>
#include <fcntl.h>
#include <unistd.h>
#define SPORT "30507"
#define MAXDATASIZE 40
#define ARRAYSIZE 20
#define BACKLOG 10	//how many pending connections queue will hold
#define FAIL "!"
#define SUCC "$"
#define BUFFERLENGTH 140

char buffer[BUFFERLENGTH];
char *serverPort;
//Parses arguments for use by program
//Makes sure no laws are broken ;)
void ParseArgs(int argc, char *argv[]){
  if(argc < 3){
    printf("Error: too few arguments given");
    exit(1);
  }
  if(argc > 3){
    printf("Error: too many arguments given");
    exit(1);
  }
  //get target port from argv
  serverPort=malloc(sizeof(argv[2]));
  serverPort=argv[2];
  if(atoi(serverPort) < 30000 || atoi(serverPort) > 40000){
    printf("Error: port number not supported");
    exit(1);
  }
}




void ServerLoop(int sockfd,int serverfd){
  int numbytes;
  char* token;
  char* delim=" \n";
  char key[MAXDATASIZE], value[MAXDATASIZE];
  char bufferCopy[BUFFERLENGTH];
  while(1){
    memset(key,0,MAXDATASIZE);
    memset(value,0,MAXDATASIZE);
    memset(buffer,0,BUFFERLENGTH);
    token=NULL;
    if((numbytes=recv(sockfd,buffer,sizeof(buffer),0))==-1){
      perror("recv");
      exit(1);
    }
    buffer[numbytes]='\0';
    strcpy(bufferCopy,buffer);

    token=strtok(buffer," ");
    printf("\t%s\n",token);
    if(strncmp(token,"quit\n",MAXDATASIZE)==0){
      if(send(serverfd,buffer,BUFFERLENGTH,0)==-1){
        perror("send");
      }
      return;

    }else if(strncmp(token,"getall\n",MAXDATASIZE)==0){
      //send the buffer to the server
      if(send(serverfd, bufferCopy, strlen(bufferCopy),0) == -1){
        perror("send");
      }

      while(1){
        int idx=0;
        int isVal=0;
        if((recv(serverfd,buffer,BUFFERLENGTH,0))==-1){
          perror("recv");
        }
        printf("%s\n",buffer);
        if(buffer[0]==FAIL[0]){
          break;
        }
        for(int i=0; i < (int)strlen(bufferCopy); i++){
          if(buffer[i]==':'){
            isVal=1;
          }
          if(isVal==1 && (buffer[i]=='c'||buffer[i]=='m'||buffer[i]=='p'||buffer[i]=='t')){
            bufferCopy[idx]=buffer[i];
            idx++;
            bufferCopy[idx]=buffer[i];
            idx++;
          }else{
            bufferCopy[idx]=buffer[i];
            idx++;
          }
        }

        if(send(sockfd,bufferCopy,BUFFERLENGTH,0)==-1){
          perror("send");
          exit(1);
        }
      }

      if(send(sockfd,FAIL,BUFFERLENGTH,0)==-1){
        perror("send");
        exit(1);
      }

    }else if(strncmp(token,"getvalue",MAXDATASIZE)==0){
      int idx=0;
      //Send the buffer to the server
      if(send(serverfd, bufferCopy, strlen(bufferCopy),0) == -1){
        perror("send");
      }

      if(recv(serverfd,bufferCopy,BUFFERLENGTH,0)==-1){
        perror("recv");
        exit(1);
      }
      if(bufferCopy[0]=='!'){
        if(send(sockfd,FAIL,1,0)==-1){
          perror("send");
          exit(1);
        }
      }
      memset(buffer,0,BUFFERLENGTH);
      for(int i=0; i < (int)strlen(bufferCopy); i++){
        if(bufferCopy[i]=='c'||bufferCopy[i]=='m'||bufferCopy[i]=='p'||bufferCopy[i]=='t'){
          buffer[idx]=bufferCopy[i];
          idx++;
          buffer[idx]=bufferCopy[i];
          idx++;
        }else{
          buffer[idx]=bufferCopy[i];
          idx++;
        }
      }
      if(send(sockfd,buffer,BUFFERLENGTH,0)==-1){
        perror("send");
        exit(1);
      }


    }else if(strncmp(token,"add",MAXDATASIZE)==0){
      if(send(serverfd, bufferCopy, BUFFERLENGTH,0) == -1){
        perror("send");
      }
      //receive the success/fail value
      if(recv(serverfd, buffer, BUFFERLENGTH,0)==-1){
        perror("recv");
        exit(1);
      }
      if(send(sockfd,buffer,1,0)==-1){
        perror("send");
        exit(1);
      }
      //TODO acknowledge

    }else if(strncmp(token,"remove",MAXDATASIZE)==0){
      token=strtok(NULL,delim);
      strcpy(key,token);
      if(send(serverfd, bufferCopy, sizeof(buffer),0) == -1){
        perror("send");
      }
      if(recv(serverfd, buffer, BUFFERLENGTH,0)==-1){
        perror("recv");
        exit(1);
      }
      if(send(sockfd,buffer,1,0)==-1){
        perror("send");
        exit(1);
      }

    }
    else{
      printf("ERROR: BUFFER TRANSFERRED INCORRECTLY\n Aquired Command: %s",token);
    }
  }       
}

//int main(int argc, char *argv[]){
int main(int argc, char *argv[]){


  int status,sockfd,clientfd,serverfd;
  socklen_t addr_size;
  struct addrinfo hints, *serverInfo;
  struct sockaddr_storage their_addr;
  struct addrinfo *servinfo;

  ParseArgs(argc,argv);
  setbuf(stdout,NULL);
  memset(&hints, 0, sizeof hints);
  hints.ai_family = AF_UNSPEC;
  hints.ai_socktype = SOCK_STREAM;
  hints.ai_flags=AI_PASSIVE;
  /* assign address info to local server */
  status = getaddrinfo(NULL, SPORT, &hints, &serverInfo);
  if (status != 0) {
    fprintf(stderr, "getaddrinfo error: %s\n", gai_strerror(status));
    exit(1);
  }
  sockfd=socket(serverInfo->ai_family,serverInfo->ai_socktype,
      serverInfo->ai_protocol);

  if(bind(sockfd,serverInfo->ai_addr,serverInfo->ai_addrlen)==-1)
  {
    close(sockfd);
    perror("server: bind");
    exit(1);
  }

  if(listen(sockfd,BACKLOG)==-1){
    perror("listen");
    exit(1);
  }



  addr_size = sizeof their_addr;
  clientfd=accept(sockfd, (struct sockaddr *)&their_addr, &addr_size);
  close(sockfd);
  //DNS/service name lookup
  //servinfo points to a linked list of addrinfo with the server info
  if((status = getaddrinfo(argv[1], serverPort, &hints, &servinfo))!=0){
    fprintf(stderr,"getaddrinfo error: %s\n", gai_strerror(status));
    exit(1);
  }

  //Setup socket
  serverfd=socket(servinfo->ai_family, servinfo->ai_socktype,
      servinfo->ai_protocol);

  if(serverfd==-1){
    perror("socket");
  }

  if(connect(serverfd, servinfo->ai_addr, servinfo->ai_addrlen)==-1){
    close(serverfd);
    perror("Client: connect");
    exit(1);
  }
  ServerLoop(clientfd,serverfd);
  freeaddrinfo(serverInfo);
  return 0;

}
